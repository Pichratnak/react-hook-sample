
// rafc  ( arrow function )
// rfc 
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState } from 'react';

function ItemCard() {

    const [title, setTitle]= useState("default")
    const [age ,setAge ]=useState(10)
    const [description ,setDescription] = useState("default")

    const handleTransform =()=>{
            setTitle(" This is the new title ")
            setDescription(" here is the description...!")
    }
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://i.pinimg.com/736x/34/49/8a/34498a8b47b70a7c23e912999c1e769b.jpg" />
      <Card.Body>
        <Card.Title>{title}  Age is : {age}</Card.Title>
        <Card.Text>
         {description}
        </Card.Text>
        <Button variant="warning" onClick={handleTransform}>Transform</Button>
      </Card.Body>
    </Card>
  );
}

export default ItemCard;